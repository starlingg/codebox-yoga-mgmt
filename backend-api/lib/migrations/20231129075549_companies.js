/* eslint-disable @typescript-eslint/no-unused-vars */
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex, Promise) {
  return knex.schema.createTable("companies", (table) => {
    table.increments("id").primary();
    table.string("company_name");
    table.string("registration_number");
    table.string("address");
    table.string("contact_number");
    table.boolean("isWithPaypal");
    table.string("facebook_url");
    table.string("instagram_url");
    table.string("linkedIn_url");
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};
  
exports.down = function (knex, Promise) {
  return knex.schema.dropTable("companies");
};
