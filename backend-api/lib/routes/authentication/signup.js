"use strict";

const Joi = require("joi");
const Helpers = require("../helpers");
const User = require("../../models/Users");

module.exports = Helpers.withDefaults({
  method: "post",
  path: "/users/signup",
  options: {
    tags: ["api"],
    validate: {
      payload: Joi.object({
        user: Joi.object()
          .required()
          .keys({
            first_name: User.field("first_name").required(),
            last_name: User.field("last_name").required(),
            email: User.field("email").required(),
            password: Joi.string().required(),
            username: User.field("username").required(),
          }),
      }),
    },
    handler: async (request, h) => {
      const { user: userInfo  } = request.payload;
      const { userService, displayService } = request.services();

      const signupAndFetchUser = async (txn) => {
        const id = await userService.signup(userInfo, txn);

        return await userService.findById(id, txn);
      }; 

      const user = await h.context.transaction(signupAndFetchUser);
      const token = userService.createToken(user.id);

      return {
        user: displayService.user(user, token)
      };
    },
  },
});
