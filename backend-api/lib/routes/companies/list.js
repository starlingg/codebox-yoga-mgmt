"use strict";

const Helpers = require("../helpers");

module.exports = Helpers.withDefaults({
  method: "get",
  path: "/companies",
  options: {
    tags: ["api"],
    auth: { strategy: "jwt", mode: "optional" },
    handler: async (request) => {
      try {
        const { companyService } = request.services();
        const response = await companyService.getCompanies();

        return response;
      } catch (error) {
        return error;
      }
    },
  }
});