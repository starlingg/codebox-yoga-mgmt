"use strict";

const Joi = require("joi");
const Helpers = require("../helpers");
const Companies = require("../../models/companies");

module.exports = Helpers.withDefaults({
  method: "post",
  path: "/companies",
  options: {
    tags: ["api"],
    validate: {
      payload: Joi.object({
        company: Joi.object().required().keys({
          company_name: Companies.field("company_name").required(),
          registration_number: Companies.field("registration_number").required(),
          address: Companies.field("address").required(),
          contact_number: Companies.field("contact_number"),
          isWithPaypal: Companies.field("isWithPaypal"),
          facebook_url: Companies.field("facebook_url"),
          instagram_url: Companies.field("instagram_url"),
          linkedIn_url: Companies.field("linkedIn_url"),
        })
      })
    },
    handler: async (request, h) => {
      try {
        const { company: companyInfo } = request.payload;
        const { companyService } = request.services();

        const createAndFetchCompany = async (txn) => {
          const { id } = await companyService.create(companyInfo, txn);
          return await companyService.findById(id, txn);
        };

        const article = await h.context.transaction(createAndFetchCompany);

        return article;
      } catch (e) {
        console.log(e);
      }
    },
  }
});