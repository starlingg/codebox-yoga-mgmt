"use strict";

const Helpers = require("../helpers");

module.exports = Helpers.withDefaults({
  method: "get",
  path: "/companies/id",
  options: {
    tags: ["api"],
    auth: "jwt",
    handler: async (request) => {
      try {
        const { companyService } = request.services();
        const response = await companyService.getCompanies();

        return response;
      } catch (error) {
        return error;
      }
    },
  }
});