"use strict";

module.exports = {
  method: "GET",
  path: "/users",
  options: {
    tags: ["api"],
    handler: async (request) => {
      try {
        const { userService } = request.services();

        const response = await userService.getUsers();

        return response;
      } catch (error) {
        return error;
      }
    },
  },
};
