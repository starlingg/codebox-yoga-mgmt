/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex("companies").del();
  await knex("companies").insert([
    {
      company_name: "Advero SRL",
      registration_number: "586KLVO",
      address: "STR. ENEAS NR. 81 300209, Timisoara Romania",
      contact_number: 46277922,
      isWithPaypal: false,
      facebook_url: "https://www.facebook.com/advero.advero/",
      instagram_url: "https://www.instagram.com/advero_trilia/",
      linkedIn_url: "https://uk.linkedin.com/in/advero-srl-global-b88485284"
    }
  ]);
};
