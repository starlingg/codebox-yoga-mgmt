/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex("users").del();
  await knex("users").insert([
    {
      first_name: "John",
      last_name: "Doe",
      email: "johndoe@gmail.com",
      username: "jdoe",
      password: "password",
    },
    {
      first_name: "Sarah",
      last_name: "Doe",
      email: "sarahdoe@gmail.com",
      username: "sdoe",
      password: "password",
    },
  ]);
};
