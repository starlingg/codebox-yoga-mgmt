"use strict";

const { Model } = require("./helpers");
const Joi = require("joi");

module.exports = class Companies extends Model {
  static get tableName() {
    return "companies";
  }

  static get joiSchema() {
    return Joi.object({
      id: Joi.number().integer(),
      company_name: Joi.string(),
      registration_number: Joi.string(),
      address: Joi.string(),
      contact_number: Joi.number().integer(),
      isWithPaypal: Joi.boolean(),
      facebook_url: Joi.string(),
      instagram_url: Joi.string(),
      linkedIn_url: Joi.string()
    });
  }
};
