"use strict";

const Schmervice = require("@hapipal/schmervice");

module.exports = class CompanyService extends Schmervice.Service {
  
  async getCompanies(txn) {
    const { Companies } = this.server.models();
    const data = await Companies.query(txn);
    
    return data;
  }

  async create(companyInfo, txn) {
    const { Companies } = this.server.models();
    const company = await Companies.query(txn).insert(companyInfo);
    console.log("Companies", company); 
    return company;
  }

  async findById(id, txn) {
    const { Companies } = this.server.models();
    const company = await Companies.query(txn).findById(id);

    return company;
  }
};
