"use strict";

const Util = require("util");
const Boom = require("@hapi/boom");
const Bounce = require("@hapi/bounce");
const Jwt = require("@hapi/jwt");
const Schmervice = require("@hapipal/schmervice");
const { UniqueViolationError } = require("objection");
const SecurePassword = require("secure-password");

module.exports = class UserService extends Schmervice.Service {

  constructor(...args) {

    super(...args);

    const pwd = new SecurePassword();
    
    this.pwd = {
      hash: Util.promisify(pwd.hash.bind(pwd)),
      verify: Util.promisify(pwd.verify.bind(pwd))
    };
  }

  async findById(id, txn) {

    const { Users } = this.server.models();

    return await Users.query(txn).throwIfNotFound().findById(id);
  }

  async findByUsername(username, txn) {

    const { Users } = this.server.models();

    return await Users.query(txn).throwIfNotFound().first().where({ username });
  }

  async follow(currentUserId, id, txn) {

    const { Users } = this.server.models();

    if (currentUserId === id) {
      throw Boom.forbidden();
    }

    try {
      await Users.relatedQuery("following", txn).for(currentUserId).relate(id);
    }
    catch (err) {
      Bounce.ignore(err, UniqueViolationError);
    }
  }

  async unfollow(currentUserId, id, txn) {

    const { Users } = this.server.models();

    if (currentUserId === id) {
      throw Boom.forbidden();
    }

    await Users.relatedQuery("following", txn).for(currentUserId)
      .unrelate().where({ id });
  }

  async signup({ password, ...userInfo }, txn) {
    const { Users } = this.server.models();
    const { id } = await Users.query(txn).insert(userInfo);

    await this.changePassword(id, password, txn);

    return id;
  }

  async update(id, { password, ...userInfo }, txn) {

    const { Users } = this.server.models();

    if (Object.keys(userInfo).length > 0) {
      await Users.query(txn).throwIfNotFound().where({ id }).patch(userInfo);
    }

    if (password) {
      await this.changePassword(id, password, txn);
    }

    return id;
  }

  async login({ email, password }, txn) {

    const { Users } = this.server.models();

    const user = await Users.query(txn).throwIfNotFound().first().where({
      email
    });

    const passwordCheck = await this.pwd.verify(Buffer.from(password), user.password);

    if (passwordCheck === SecurePassword.VALID_NEEDS_REHASH) {
      await this.changePassword(user.id, password, txn);
    }
    else if (passwordCheck !== SecurePassword.VALID) {
      throw Users.createNotFoundError();
    }

    return user;
  }

  createToken(id) {

    return Jwt.token.generate({ id }, {
      key: this.options.jwtKey,
      algorithm: "HS256"
    }, {
      ttlSec: 7 * 24 * 60 * 60 // 7 days
    });
  }

  async changePassword(id, password, txn) {
    const { Users } = this.server.models();

    await Users.query(txn).throwIfNotFound().where({ id }).patch({
      password: await this.pwd.hash(Buffer.from(password))
    });

    return id;
  }
};
